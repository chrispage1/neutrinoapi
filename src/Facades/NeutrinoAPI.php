<?php

namespace Chrispage1\NeutrinoAPI\Facades;

use Illuminate\Support\Facades\Facade;

class NeutrinoAPI extends Facade {

    protected static function getFacadeAccessor() {
        return 'neutrinoapi';
    }

}