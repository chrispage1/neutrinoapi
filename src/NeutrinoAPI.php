<?php

namespace Chrispage1\NeutrinoAPI;

class NeutrinoAPI {

    const
        METHOD_GET = 'GET',
        METHOD_POST = 'POST';

    protected
        $client,
        $user_id,
        $api_key,
        $base_uri = 'https://neutrinoapi.com/';

    /**
     * Neutrino constructor.
     * @param object $client
     * @param string $user_id
     * @param string $api_key
     */
    public function __construct($client, $user_id, $api_key) {

        // create guzzle client
        $this->client = $client;

        // setup user id & api key
        $this->user_id = $user_id;
        $this->api_key = $api_key;
    }


    /**
     * @param string $endpoint
     * @param array $request
     * @param string $type
     * @return bool|mixed
     */
    protected function _apiCall ($endpoint, $request = [], $type = self::METHOD_GET) {

        // add required params to our request
        $request = array_merge($request, [
            'user-id' => $this->user_id,
            'api-key' => $this->api_key,
            'output-format' => 'JSON',
            'output-case' => 'camel',
        ]);

        try {

            // run our response
            $response = $this->client->request($type, $this->base_uri . (string)$endpoint, [
                'query' => $request,
            ]);

            // return our json body
            return json_decode($response->getBody());

        } catch (GuzzleException $e) { /* do nothing */ }

        // return false
        return false;
    }


    /**
     * Determines default IP address to query
     * @param null|string $ip
     * @return string
     */
    private function _determineIp($ip = null) {
        return is_null($ip) ? $_SERVER['REMOTE_ADDR'] : $ip;
    }


    /**
     * Parse, validate and clean an email address.
     *
     * @param string $email
     * @param bool $fixTypos
     * @see https://www.neutrinoapi.com/api/email-validate/
     * @return bool|mixed
     */
    public function emailValidate($email, $fixTypos = false) {
        return $this->_apiCall('email-validate', [
            'email' => (string)$email,
            'fix-typos' => (bool)$fixTypos,
        ]);
    }


    /**
     * SMTP based email address verification. Verify real users and filter out low-quality email addresses.
     *
     * @param string $email
     * @param bool $fixTypos
     * @see https://www.neutrinoapi.com/api/email-verify/
     * @return bool|mixed
     */
    public function emailVerify($email, $fixTypos = false) {
        return $this->_apiCall('email-verify', [
            'email' => (string)$email,
            'fix-typos' => (bool)$fixTypos,
        ]);
    }

    /**
     * Parse, validate and get location information about a phone number.
     *
     * @param string $number
     * @param null $countryCode
     * @param null $ip
     * @see https://www.neutrinoapi.com/api/phone-validate/
     * @return bool|mixed
     */
    public function phoneValidate($number, $countryCode = null, $ip = null) {

        return $this->_apiCall('phone-validate', [
            'number' => (string)$number,
            'country-code' => (string)$countryCode,
            'ip' => (string)$ip,
        ]);
    }

    /**
     * Parse, validate and get detailed user-agent information from a user agent string.
     *
     * @param null|string $userAgent
     * @see https://www.neutrinoapi.com/api/user-agent-info/
     * @return bool|mixed
     */
    public function userAgentInfo ($userAgent = null) {

        // get our user agent
        if(is_null($userAgent) && array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        }

        // return our api call
        return $this->_apiCall('user-agent-info', [
            'user-agent' => (string)$userAgent
        ]);
    }


    /**
     * Clean and sanitize untrusted HTML.
     *
     * @param string $content
     * @param string $outputType
     * @see https://www.neutrinoapi.com/api/html-clean/
     * @return bool|mixed
     */
    public function htmlClean($content, $outputType = 'plain-text') {
        return $this->_apiCall('html-clean', [
            'content' => (string)$content,
            'output-type' => (string)$outputType,
        ]);
    }


    /**
     * Extract specific HTML tag contents or attributes from complex HTML or XHTML content.
     *
     * @param string $content
     * @param string $tag
     * @param null|string $attribute
     * @param null|string $baseUrl
     * @see https://www.neutrinoapi.com/api/html-extract-tags/
     * @return bool|mixed
     */
    public function htmlExtractTags ($content, $tag, $attribute = null, $baseUrl = null) {
        return $this->_apiCall('html-extract-tags', [
            'content' => (string)$content,
            'tag' => (string)$tag,
            'attribute' => (string)$attribute,
            'base-url' => (string)$baseUrl,
        ]);
    }

    /**
     * Detect bad words, swear words and profanity in a given text.
     *
     * @param string $content
     * @param string $censorCharacter
     * @see https://www.neutrinoapi.com/api/bad-word-filter/
     * @return bool|mixed
     */
    public function badWordFilter ($content, $censorCharacter = '*') {
        return $this->_apiCall('bad-word-filter', [
            'content' => (string)$content,
            'censor-character' => (string)$censorCharacter,
        ]);
    }

    /**
     * Code highlight will take raw source code and convert into nicely formatted HTML.
     *
     * @param string $content
     * @param string $type
     * @param bool $addKeywordLinks
     * @see https://www.neutrinoapi.com/api/code-highlight/
     * @return bool|mixed
     */
    public function codeHighlight ($content, $type = 'PHP', $addKeywordLinks = false) {
        return $this->_apiCall('code-highlight', [
            'content' => (string)$content,
            'type' => (string)$type,
            'add-keyword-links' => (bool)$addKeywordLinks,
        ]);
    }

    /**
     * Convert most known measurement types: currency, imperial, metric, mass, length, temperature, time (and more).
     * The converter will accept full unit names as well as unit abbreviations (e.g. millimeter and mm).
     *
     * @param string $value
     * @param string $from
     * @param string $to
     * @see https://www.neutrinoapi.com/api/convert/
     * @return bool|mixed
     */
    public function convert ($value, $from = 'GBP', $to = 'USD') {
        return $this->_apiCall('convert', [
            'from-value' => (float)$value,
            'from-type' => (string)$from,
            'to-type' => (string)$to,
        ]);
    }


    /**
     * Make an automated call to any valid phone number and playback a unique security code.
     *
     * @param string $number
     * @param int $codeLength
     * @param null $securityCode
     * @param int $playbackDelay
     * @param null $countryCode
     * @param string $languageCode
     * @see https://www.neutrinoapi.com/api/phone-verify/
     * @return bool|mixed
     */
    public function phoneVerify ($number, $codeLength = 6, $securityCode = null, $playbackDelay = 800, $countryCode = null, $languageCode = 'en') {
        return $this->_apiCall('phone-verify', [
            'number' => (string)$number,
            'code-length' => (int)$codeLength,
            'security-code' => (!$securityCode ?: (int)$securityCode),
            'playback-delay' => (int)$playbackDelay,
            'country-code' => (string)$countryCode,
            'language-code' => (string)$languageCode,
        ]);
    }


    /**
     * Send a unique security code to any mobile device via SMS.
     *
     * @param string $number
     * @param int $codeLength
     * @param null $securityCode
     * @param null $countryCode
     * @param string $languageCode
     * @see https://www.neutrinoapi.com/api/sms-verify/
     * @return bool|mixed
     */
    public function smsVerify($number, $codeLength = 5, $securityCode = null, $countryCode = null, $languageCode = 'en') {
        return $this->_apiCall('sms-verify', [
            'number' => (string)$number,
            'code-length' => (int)$codeLength,
            'security-code' => (!$securityCode ?: (int)$securityCode),
            'country-code' => (string)$countryCode,
            'language-code' => (string)$languageCode,
        ]);
    }


    /**
     * Check if a security code from one of the verify APIs is valid.
     *
     * @param string $securityCode
     * @see https://www.neutrinoapi.com/api/verify-security-code/
     * @return bool|mixed
     */
    public function verifySecurityCode($securityCode) {
        return $this->_apiCall('verify-security-code', [
            'security-code' => (string)$securityCode,
        ]);
    }


    /**
     * Make an automated call to any valid phone number and playback an audio message.
     *
     * @param string $number
     * @param string $audioUrl
     * @see https://www.neutrinoapi.com/api/phone-playback/
     * @return bool|mixed
     */
    public function phonePlayback ($number, $audioUrl) {
        return $this->_apiCall('phone-playback', [
            'number' => (string)$number,
            'audio-url' => (string)$audioUrl,
        ]);
    }


    /**
     * Send a free-form message to any mobile device via SMS.
     *
     * @param string $number
     * @param string $message
     * @param null|string $countryCode
     * @see https://www.neutrinoapi.com/api/sms-message/
     * @return bool|mixed
     */
    public function sendSmsMessage ($number, $message, $countryCode = null) {
        return $this->_apiCall('sms-message', [
            'number' => (string)$number,
            'message' => (string)$message,
            'country-code' => (string)$countryCode
        ]);
    }


    /**
     * Connect to the global mobile cellular network and retrieve the status of a mobile device.
     *
     * @param string $number
     * @param null|string $countryCode
     * @see https://www.neutrinoapi.com/api/hlr-lookup/
     * @return bool|mixed
     */
    public function hlrLookup ($number, $countryCode = null) {
        return $this->_apiCall('hlr-lookup', [
            'number' => (string)$number,
            'country-code' => (string)$countryCode,
        ]);
    }


    /**
     * Get location information about an IP address and do reverse DNS (PTR) lookups.
     *
     * @param null|string $ip
     * @param bool $reverseLookup
     * @see https://www.neutrinoapi.com/api/ip-info/
     * @return bool|\stdClass
     */
    public function ipInfo ($ip = null, $reverseLookup = false) {

        return $this->_apiCall('ip-info', [
            'ip' => (string)$this->_determineIp($ip),
            'reverseLookup' => (bool)$reverseLookup,
        ]);
    }


    /**
     * Analyze and extract provider information for an IP address.
     *
     * @param null|string $ip
     * @see https://www.neutrinoapi.com/api/ip-probe/
     * @return bool|mixed
     */
    public function ipProbe ($ip = null) {
        return $this->_apiCall('ip-probe', [
            'ip' => (string)$this->_determineIp($ip)
        ]);
    }


    /**
     * The IP Blocklist API will detect potentially malicious or dangerous IP addresses.
     *
     * @param null|string $ip
     * @see https://www.neutrinoapi.com/api/ip-blocklist/
     * @return bool|mixed
     */
    public function ipBlocklist($ip = null) {
        return $this->_apiCall('ip-blocklist', [
            'ip' => (string)$this->_determineIp($ip)
        ]);
    }


    /**
     * Check the reputation of an IP address OR domain against a comprehensive list of blacklists and blocklists.
     *
     * @param string|null $host
     * @param int $listRating
     * @see https://www.neutrinoapi.com/api/host-reputation/
     * @return bool|mixed
     */
    public function hostReputation ($host = null, $listRating = 3) {
        return $this->_apiCall('host-reputation', [
            'host' => (string)$this->_determineIp($host),
            'list-rating' => (int)$listRating,
        ]);
    }


    /**
     * Parse, analyze and retrieve content from the supplied URL.
     *
     * @param string $url
     * @param bool $fetchContent
     * @see https://www.neutrinoapi.com/api/url-info/
     * @return bool|mixed
     */
    public function urlInfo($url, $fetchContent = false) {
        return $this->_apiCall('url-info', [
            'url' => $url,
            'fetch-content' => (bool)$fetchContent,
        ]);
    }


    /**
     * Geocode an address, partial address or just the name of a place.
     *
     * @param string $address
     * @param null $countryCode
     * @param null $languageCode
     * @param bool $fuzzySearch
     * @see https://www.neutrinoapi.com/api/geocode-address/
     * @return bool|mixed
     */
    public function geocodeAddress ($address, $countryCode = null, $languageCode = null, $fuzzySearch = false) {
        return $this->_apiCall('geocode-address', [
            'address' => (string)$address,
            'country-code' => (string)$countryCode,
            'language-code' => (string)$languageCode,
            'fuzzySearch' => (bool)$fuzzySearch,
        ]);
    }

    /**
     * Convert a geographic coordinate (latitude and longitude) into a real world address or location.
     *
     * @param string|float $latitude
     * @param string|float $longitude
     * @param null $languageCode
     * @see https://www.neutrinoapi.com/api/geocode-reverse/
     * @return bool|mixed
     */
    public function geocodeReverse ($latitude, $longitude, $languageCode = null) {
        return $this->_apiCall('geocode-reverse', [
            'latitude' => (float)$latitude,
            'longitude' => (float)$longitude,
            'language-code' => (string)$languageCode,
        ]);
    }


    /**
     * Perform a BIN (Bank Identification Number) or IIN (Issuer Identification Number) lookup.
     *
     * @param string $binNumber
     * @param null $customerIp
     * @return bool|mixed
     */
    public function binLookup ($binNumber, $customerIp = null) {
        return $this->_apiCall('bin-lookup', [
            'bin-number' => (string)$binNumber,
            'customer-ip' => $this->_determineIp($customerIp),
        ]);
    }


    /**
     * Convert currency using an up-to-date international currency feed.
     *
     * @param float $value
     * @param string|null $from Currency code
     * @param string|null $to Currency code
     * @see https://www.neutrinoapi.com/api/convert/
     * @return bool|mixed
     */
    public function currencyConvert ($value, $from = 'USD', $to = 'GBP') {
        return $this->convert($value, $from, $to);
    }
}