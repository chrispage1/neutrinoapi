<?php

namespace Chrispage1\NeutrinoAPI\Exceptions;

class InvalidConfiguration extends \Exception {

    public static function configurationNotSet () {
        return new static('You need to add Neutrino configuration to your services configuration file');
    }
}