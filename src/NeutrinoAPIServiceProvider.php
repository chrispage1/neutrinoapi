<?php

namespace Chrispage1\NeutrinoAPI;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class NeutrinoAPIServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register () {

        $this->app->singleton('neutrinoapi', function () {

            $config = config('services.neutrino');

            if(is_null($config)) {
                throw InvalidConfiguration::configurationNotSet();
            }

            // return new instance
            return new NeutrinoAPI(new Client(), config('services.neutrino.user_id'), config('services.neutrino.api_key'));
        });

    }
}
